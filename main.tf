provider "aws" {
  region = "ap-south-1"
  access_key = "AKIAVP7FDJF4UU25BS6M"
  secret_key = "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"
}




resource "aws_vpc" "development-vpc" {
cidr_block = "10.0.0.0/16"
tags ={
  Name : "dev-vpc"
}
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.var_subnet
  availability_zone = var.availability_zone
  tags = {
    Name : "dev-subnet"
  }
}


resource "aws_route_table" "route_table" {
vpc_id =aws_vpc.development-vpc.id
route{
cidr_block ="0.0.0.0/0"
gateway_id =aws_internet_gateway.aws_gateway.id
}
tags ={
  Name: "aws_route_table_yash"
}
}


resource "aws_internet_gateway" "aws_gateway" {
vpc_id=aws_vpc.development-vpc.id
tags ={
  Name: "aws_gateway_yash"
}
}

resource "aws_route_table_association" "rt_subnet" {
  subnet_id = aws_subnet.dev-subnet-1.id
  route_table_id = aws_route_table.route_table.id
}



resource "aws_instance" "web" {
  ami="ami-0376ec8eacdf70aae"
  instance_type="t2.micro"
  tags = {
    Name= "Terraform Ec2"
  }

  key_name = aws_key_pair.ssh-key.key_name

  user_data = file("entry-script.sh")
}

resource "tls_private_key" "example_keypair" {

algorithm = "RSA"

rsa_bits = 2048

}

resource "aws_key_pair" "ssh-key" {

key_name = "server-key"

public_key = tls_private_key.example_keypair.public_key_openssh

}

resource "aws_default_security_group" "sec_group" {
  
  vpc_id =aws_vpc.development-vpc.id

  ingress{
    from_port =22
    to_port=22
    protocol="tcp"
    cidr_blocks=[var.my_ip]
  }

  
  ingress{
    from_port =8080
    to_port=8080
    protocol="tcp"
    cidr_blocks=["0.0.0.0/0"]
  }

   egress{
    from_port =0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
    prefix_list_ids=[]
  }
}


resource "aws_security_group" "sec_group_test" {
  
  name = "Hello"


  
  ingress{
    from_port =0
    to_port=0
    protocol="all"
    cidr_blocks=["0.0.0.0/0"]
  }

   egress{
    from_port =0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
    prefix_list_ids=[]
  }
}



output "dev-vpc" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet" {
  value= aws_subnet.dev-subnet-1.id
}